import java.util.Arrays;

public class Wrapper<T> {
	private int capacity=10;
	private int size=0;
	private Object data[]= {};
	public Wrapper() {
		data=new Object[capacity];
	}
	public Wrapper(int capacity) {
		data=new Object[capacity];
	}
	public void addItem(T e) throws NullPointerException,DuplicateElementException{
		if(e==null)
			throw new NullPointerException("Inputted Null Value");
		if(this.contains(e))
			throw new DuplicateElementException("Duplicate Value : "+e);
		if(size==capacity) {
			capacity*=2;
			data=Arrays.copyOf(data,capacity);
		}
		data[size++]=e;
	}
	public T getItem(int index){
		return (T)data[index];
		
	}
	public int getSize() {
		return size;
	}
	public boolean contains(T e) {
		for(int i=0;i<size;i++) {
			if(e==data[i])
				return true;
		}
		return false;
	}
}
